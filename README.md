## Использование

1. Установите все необходимые зависимости на все ноды:

```bash
ansible-playbook -i hosts k8s-dependencies.yml
```

2. Инициализируйте Kubernetes control plane ноду:

```bash
ansible-playbook -i hosts k8s-control-plane.yml
```

3. Присоедините worker ноды к кластеру:

```bash
ansible-playbook -i hosts k8s-workers.yml
```
